from pytriqs.gf.local import BlockGf, GfImFreq, GfImTime, rebinning_tau
from bryan import Bryan, MeasureFunctor
from rebin import Rebinner
import numpy

class MaximumEntropy(object):
    """Initialized by a TRIQS BlockGf on either imaginary frequencies or times, calculates the trace and then the analytical continuation to real frequencies and thereby the Green's function's total density of states. Fermionic only!"""
    def __init__(self, gin, rebinTauPoints = False, nTau = 10001):
        self.gin = gin
        self.indices = list()
        for ind in gin.indices:
            self.indices.append(ind)
        self.beta = gin[self.indices[0]].beta
        self.nTau = nTau
        self.rebinTauPoints = rebinTauPoints

    def calculateTotDOS(self, nOmega, bandwidth, sigma = None, sigma_threshold = None):
        if type(self.gin[self.indices[0]]) == GfImFreq:
            self._setTrGiw()
            self._setTrGTauByFT(self.nTau)
        else:
            assert type(self.gin[self.indices[0]]) == GfImTime, 'BlockGf of either GfImFreq or GfImTime expected'
            self._setTrGTauByTr()
        if self.rebinTauPoints:
            reb = Rebinner(self.trGTau, self.rebinTauPoints)
            self.trGTau = reb.g_rebinned
            if sigma is None:
                sigma = list(reb.g_error.data[:,0,0].real)
                print 'min(sigma): ', min(sigma), 'at', numpy.argmin(sigma)
                print 'max(sigma): ', max(sigma), 'at', numpy.argmax(sigma)
                mean_sigma = numpy.mean(sigma)
                print 'mean(sigma):', mean_sigma
                sigma = mean_sigma
                print 'using sigma =', sigma, '= const.' #otherwise very unstable
                #if sigma_threshold is None:
                #    sigma_threshold = mean_sigma
                #print 'threshold(sigma):', sigma_threshold
                #for i, sig in enumerate(sigma):
                #    sigma[i] = max(sig, sigma_threshold)
        self.GTau = self.trGTau
        self.calculateDOS(nOmega, bandwidth, sigma, False)

    def calculateDOS(self, nOmega, bandwidth, sigma, orbital):
        """orbital is a 3-tuple, with block, row and column index"""
        if orbital:
            if type(self.gin[self.indices[0]]) == GfImFreq:
                self._setGTauByFT(orbital, self.nTau)
            else:
                assert type(self.gin[self.indices[0]]) == GfImTime, 'BlockGf of either GfImFreq or GfImTime expected'
                self.GTau = self.gin[orbital[0]]
        else:
            orbital = ['', 0, 0]
        g = list()
        tau_mesh = list()
        for i, tau in enumerate(self.GTau.mesh):
            g.append(self.GTau.data[i, orbital[1], orbital[2]].real)
            tau_mesh.append(tau.real)
        G = numpy.array(g, numpy.float64)
        
        NOmega = nOmega
        self.tauMesh = tauMesh = numpy.array(tau_mesh, numpy.float128)
        Beta=tauMesh[0]+tauMesh[-1]
        self.omegaMesh = omegaMesh = numpy.linspace(-float(bandwidth)/2.0,float(bandwidth)/2.0,int(NOmega))
        if type(sigma) == list:
            assert len(sigma) == len(tauMesh), 'sigma and tauMesh have to be of equal length'
            sigmapm2 = [1.0 / float(sig)**2 for sig in sigma]
        else:
            sigmapm2 = [1.0 / float(sigma)**2 for tau in tauMesh]
        sigmapm2 = numpy.array(sigmapm2, numpy.float64)
        model=numpy.ones(omegaMesh.shape, numpy.float64)
        model/=numpy.trapz(model,omegaMesh)
        B=Bryan(tauMesh, omegaMesh, Beta, model)
        outputA = list()
        B.initG(G, sigmapm2)
        (aMax,AMax,S,L,lam)=B.approxMax()
        F=MeasureFunctor(tauMesh,omegaMesh,(0.5*numpy.sum(numpy.log(aMax/(aMax+lam)))+aMax*S-L)-3, aMax, AMax, S, L, lam,G)
        B.screen(numpy.log(aMax), 0.025, AMax, F.add)
        B.screen(numpy.log(aMax), -0.025, AMax, F.add)
        ABryan=F.finalize()
        outputA.append(numpy.copy(ABryan))
        if orbital[0]:
            self.DOS = numpy.array(*outputA)
        else:
            self.totDOS = numpy.array(*outputA)
        self.gMaxEnt = list()
        for i in range(len(tauMesh)):
            self.gMaxEnt.append(-numpy.dot(B.K[i,:], outputA[0]))

    def _setTrGiw(self):
        self.trGiw = GfImFreq(indices = [0], mesh = self.gin.mesh)
        n_orb = self._n_orb(self.gin)
        for s, b in self.gin:
            for i in range(len(b.data[0, :, :])):
                self.trGiw += b[i, i] / n_orb

    def _setTrGTauByFT(self, nTau):
        self.trGTau = GfImTime(indices = [0], beta = self.beta, n_points = nTau)
        self.trGTau.set_from_inverse_fourier(self.trGiw)

    def _setTrGTauByTr(self):
        self.trGTau = GfImTime(indices = [0], mesh = self.gin[self.indices[0]].mesh)
        n_orb = self._n_orb(self.gin)
        for s, b in self.gin:
            for i in range(len(b.data[0, :, :])):
                self.trGTau += b[i, i] / n_orb

    def _n_orb(self, g):
        n = 0
        for s, b in g:
            for i in range(len(b.data[0,:,:])):
                n += 1
        return n

    def getTotDOS(self):
        return self.totDOS

    def getDOS(self):
        return self.DOS

    def getOmegaMesh(self):
        return self.omegaMesh

    def _setGTauByFT(self, orbital, nTau):
        self.GTau = GfImTime(indices = [0], beta = self.beta, n_points = nTau)
        for s, b in self.gin:
            if s == orbital[0]:
                self.GTau.set_from_inverse_fourier(b)
