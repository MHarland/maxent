#!/usr/bin/env pytriqs
from pytriqs.gf.local import GfImTime, BlockGf
from bryanToTRIQS import MaximumEntropy
from matplotlib import pyplot as plt
import numpy as np

gData = np.loadtxt('Gtau.dat', np.float128)[:, 1]
nTau = len(gData)
upBlock = GfImTime(indices = [0], beta = 10, n_points = nTau)
upBlock.data[:, 0, 0] = gData
g = BlockGf(name_list = ['up'], block_list = [upBlock])

maxent = MaximumEntropy(g, nTau)
maxent.calculateTotDOS(700, 20, .003)

fig = plt.figure()
ax = fig.add_subplot(1,1,1)
ax.plot(maxent.getOmegaMesh(), maxent.getTotDOS())
plt.savefig('ex_bryanToTRIQS.pdf')
