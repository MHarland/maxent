from matplotlib import pyplot as plt
from numpy import loadtxt

datas = [loadtxt('Gtau.dat_dos.dat'), loadtxt('Gtau.dat_dos_nomoments.dat')]
titles = ['moments', 'no moments']
fig = plt.figure()
for i, data, title in zip(range(len(datas)), datas, titles):
    ax = fig.add_subplot(1,2,i+1)
    ax.plot(data[:,0], data[:,1])
    ax.set_title(title)
plt.savefig('gtauDatDos.pdf')
