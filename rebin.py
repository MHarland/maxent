import numpy as np
from scipy import stats
from pytriqs.gf.local import GfImTime


class Rebinner:
    """
    http://www.cond-mat.de/events/correl12/manuscripts/jarrell.pdf
    Replaces a set of old datapoints by their mean, also calculating the relative 
    standard error of the mean. The sets are such, that their tau points are mapped to 
    the closest of the new mesh.
    """
    def __init__(self, g_tau, n_tau):
        self.g_rebinned = GfImTime(beta = g_tau.beta, indices = [i for i in g_tau.indices], n_points = n_tau)
        self.g_error = GfImTime(beta = g_tau.beta, indices = [i for i in g_tau.indices], n_points = n_tau)
        self.mesh_map = {}
        self._set_tail(g_tau)
        self._create_mesh_map(g_tau)
        self._rebin(g_tau)

    def _set_tail(self, g):
        self.g_rebinned.tail = g.tail

    def _create_mesh_map(self, g):
        mesh = np.array([t for t in g.mesh])
        mesh_rebinned = np.array([t for t in self.g_rebinned.mesh])
        for i, tau in enumerate(mesh):
            mesh_dist = np.array([abs(tau2 - tau) for tau2 in mesh_rebinned])
            i_closest_tau = np.argmin(mesh_dist)
            if i_closest_tau in self.mesh_map.keys():
                self.mesh_map[i_closest_tau].append(i)
            else:
                self.mesh_map[i_closest_tau] = [i]

    def _rebin(self, g):
        for i_tau_rebinned, i_taus in self.mesh_map.items():
            mean = np.mean([g.data[i_tau2,:,:] for i_tau2 in i_taus], axis = 0)
            self.g_rebinned.data[i_tau_rebinned,:,:] = mean
            self.g_error.data[i_tau_rebinned,:,:] = stats.sem([g.data[i_tau2,:,:] for i_tau2 in i_taus], axis = 0, ddof = 0, nan_policy = 'raise') / abs(mean)
